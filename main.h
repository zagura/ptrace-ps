//
// Created by misiek on 23.06.17.
//

#ifndef _MAIN_H
#define _MAIN_H
#define _GNU_SOURCE
#define _XOPEN_SOURCE 700

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <stdbool.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "ansible.h"
#include "plog.h"
#include "help.h"
#include "trace.h"


static const struct option long_options[] = {
        {"verbose",   no_argument,       0, 'v'},
        {"help",      no_argument,       0, 'h'},
        {"pid",       required_argument, 0, 'p'},
        {"directory", required_argument, 0, 'd'},
        {"log",       required_argument, 0, 'l'},
        {"output",    required_argument, 0, 'o'},
        {0, 0,                           0, 0}
};

static const char *short_opts = "vhp:d:l:o:";
static char *role_dir = NULL;
static char *process_name = NULL;
static char **process_args = NULL;
static char *log_filename = NULL;
static FILE *log_file = NULL;
static char* output_filename = NULL;
void exit_cleanup(void);

bool is_dir(const char *path);

bool is_regfile(const char *path);

#endif //PTRACE_PS_MAIN_H
