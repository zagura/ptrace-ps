//
// Created by misiek on 24.06.17.
//

#ifndef _ANSIBLE_H
#define _ANSIBLE_H

#include <stdio.h>

#include <stdlib.h>
#include <linux/limits.h>
#include "plog.h"


FILE* ansible_open(const char* dir, FILE* log, const char* output);
void ansible_init(FILE* file, FILE* log);
void ansible_file_config(FILE* log, char* path, int mode);
#endif //PTRACE_PS_ANSIBLE_H
