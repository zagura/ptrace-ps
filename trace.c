//
// Created by misiek on 23.06.17.
//


#include "trace.h"
#include <sys/prctl.h>
#include <sys/user.h>


const unsigned int syscall_trap_sig = SIGTRAP | 0x80;
void grand_cap(FILE *log);

void follow(pid_t tracee, FILE* log) {
    long pres = 0;
    int status = 0;
    unsigned long long int syscall_num = 5000;
    struct user_regs_struct uregs;
    pres = ptrace(PTRACE_SETOPTIONS, tracee, 0, PTRACE_O_TRACESYSGOOD);
    if (pres == -1) {
        log_format(plog_err, log, "Cannot set options. %s" ,strerror(errno));
    }
    while(true) {
        ptrace(PTRACE_SYSCALL, tracee, 0, 0);
        waitpid(tracee, &status, 0);
//        log_format(plog_debug,log, "Wait result %d", status);
        if(WIFEXITED(status))
            return;
        if(WIFSTOPPED(status) && (WSTOPSIG(status) & 0x80)) {
//            log_format(plog_debug, log, "%d ", __LINE__);
            if(WSTOPSIG(status) == syscall_trap_sig){
//                log_format(plog_debug, log, "Inner loop.");

            // Take care of full syscall ...
            // TODO: File syscalls
            // TODO: Network syscalls
            // TODO:
            //syscall_num = ptrace(PTRACE_PEEKUSER, tracee, 0, sizeof(long)*ORIG_RAX);
                ptrace(PTRACE_GETREGS, tracee, 0, &uregs);
#ifdef __i386__
                syscall_num = uregs.orig_eax;
#elif __x86_64__
                syscall_num = uregs.orig_rax;
#else
                errno = ENOSYS;
                log_format(plog_err, log, "Unimplemented architecture: %s", strerror(errno));
                exit(EXIT_FAILURE);
#endif
                char *name = get_sys_name(syscall_num);
                log_format(plog_debug, log, "Syscall number: 0x%llx (%s).", syscall_num, name);
                handle_syscall(syscall_num, tracee, log,&uregs);
                log_format(plog_debug, log, "Registers:\n"
                        "\tax\t0x%llx\n"
                        "\tbx\t0x%llx\n"
                        "\tcx\t0x%llx\n"
                        "\tdx\t0x%llx\n"
                        "\tip\t0x%llx\n"
                        "\tsp\t0x%llx\n"
                        "\tbp\t0x%llx\n", uregs.rax ,uregs.rbx , uregs.rcx, uregs.rdx, uregs.rip, uregs.rsp, uregs.rbp);
                if (name != NULL)
                    free(name);
                name = NULL;
            }
        }
    }
}



void trace_by_pid(pid_t pid, FILE *log)
{
    long pres = 0;
    log_format(plog_debug, log, "Start trace_by_pid with %d arg.", pid);
    if (pid == getpid()) {
        plog_warn(log, "Cannot start to trace myself. Exiting...");
        exit(-EINVAL);
    }

    //grand_cap(log);

    pres = ptrace(PTRACE_ATTACH, pid, 0, 0);
    if (pres == -1) {
        log_format(plog_err, log, "Pid %d attach failed: %s", pid, strerror(errno));
        return;
    }
    follow(pid, log);
}

void grand_cap(FILE *log) {
    if (prctl(PR_MCE_KILL, PR_MCE_KILL_SET, PR_MCE_KILL_DEFAULT, 0,0) == -1) {
        plog_debug(log, "Cannot set kill cap.");
    }
    plog_debug(log, "Trying to increase caps.");
    if (prctl(PR_SET_PTRACER, PR_SET_PTRACER_ANY, 0, 0, 0) == -1) {
        log_format(plog_err, log, "Cannot update caps. %s", strerror(errno));
    }
}

void trace_by_name(const char *name, char **args, FILE *log)
{
    int status;
    long pres = 0;

    pid_t pid = fork();
    if (pid == 0) {
        pres = ptrace(PTRACE_TRACEME, getpid(), 0,0);
        if (pres == -1) {
            log_format(plog_debug, log, "Process %s attach failed: %s", name, strerror(errno));
        }
        else {
            log_format(plog_debug, log, "Successfully attached %s.", name);
        }
        kill(getpid(), SIGSTOP);
        execvp(name, args);
    }
    if (pid > 0) {
        log_format(plog_debug, log, "Forked new process %s with pid %d.", name, pid);
        waitpid(0, &status, 0);
        if (!WIFSTOPPED(status)) {
            log_format(plog_warn, log, "Unexpected child status instead of SIGSTOP. %d", status);
        }
        log_format(plog_debug, log, "Process %s sent status %d.", name, status);
        follow(pid, log);
/*        while(1) {
            if (wait_for_syscall(pid) != 0) break;

            pres = ptrace(PTRACE_PEEKUSER, pid, sizeof(long)*ORIG_RAX);
            fprintf(log, "syscall(%ld) = ", pres);

            if (wait_for_syscall(pid) != 0) break;

            pres = ptrace(PTRACE_PEEKUSER, pid, sizeof(long)*RAX);
            fprintf(log, "%ld\n", pres);
        }
        log_format(plog_debug, log, "Proces %s returned status %d.", name, status);*/
    }
}



void check_arch(FILE* log) {

#ifdef __i386__
    plog_debug(log, "Architecture i386.");
#elif __x86_64__
    plog_debug(log, "Architecture x86_64.");
#else
    plog_debug(log, "Unknown architecture.");
#endif
}

