#ifndef _PLOG_H
#define _PLOG_H


#define _GNU_SOURCE
#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <unistd.h>

#define LOG_LEN 180
extern char log_tmp[LOG_LEN];
#define log_format(_log, file, ...)                 \
        do{                                         \
        snprintf(log_tmp, LOG_LEN, __VA_ARGS__);    \
        _log(file, log_tmp);                        \
        }while(0)                                   \

extern bool verbose;

void plog_warn(FILE *file, const char *msg);

void plog_info(FILE *file, const char *msg);

void plog_err(FILE *file, const char *msg);

void plog_debug(FILE *file, const char *msg);

void plog_init(FILE* file);

#endif
