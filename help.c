#include "help.h"

void get_help()
{
    fprintf(stdout, "Generating ansible config based on `ptrace` - Help:\n");
    fprintf(stdout, "\nmain [opts] program name [program args]\n");
    fprintf(stdout, "main [opts] -p | --pid PID\n\n");
    fprintf(stdout, "Options:\n");
    fprintf(stdout, "\t-h, --help\t print this text,\n");
    fprintf(stdout, "\t-p, --pid PID\t choose traced process by PID, ommiting program name and it's args\n");
    fprintf(stdout, "\t-l, --log FILE\t choose log file, default logs are printed to stderr,\n");
    fprintf(stdout, "\t-d, --dir DIR\t set output directory for ansible tasks,\n");
    fprintf(stdout, "\t-v, --verbose\t verbose mode - more logs send to log file,\n");
    fprintf(stdout, "\t-o, --output\t set output concrete tasks filename,\n");
    fprintf(stdout, "\n\tprogram name\t name of program called to be traced,\n");
    fprintf(stdout, "\tprogram args\t arguments for called program\n");
}