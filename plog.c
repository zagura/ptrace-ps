#include "plog.h"

char log_tmp[LOG_LEN];
bool verbose = false;
void plog(FILE *file, const char *msg, const char *tag)
{
    time_t now;
    time(&now);
    char* timestamp = strdupa(ctime(&now));
    timestamp[strlen(timestamp) - 1] = '\0';
    fprintf(file, "%s [%s]: %s\n", timestamp, tag, msg);
    fflush(file);
}

void plog_err(FILE *file, const char *msg)
{
    plog(file, msg, "ERROR");
}

void plog_warn(FILE *file, const char *msg)
{
    plog(file, msg, "WARN");
}

void plog_debug(FILE *file, const char *msg)
{
    if (verbose)
        plog(file, msg, "DEBUG");
}

void plog_info(FILE *file, const char *msg)
{
    if (verbose)
        plog(file, msg, "INFO");
}

void plog_init(FILE *file){
    time_t now;
    time(&now);
    /* 80 '-' chars in line to set the barrier on each init of logging in the same file */
    fprintf(file, "----------------------------------------------------------------\n");
    fprintf(file, "%s", ctime(&now));
    fprintf(file, "----------------------------------------------------------------\n");
    fflush(file);
}
