//
// Created by misiek on 23.06.17.
//

#ifndef _TRACE_H
#define _TRACE_H

#include "plog.h"
#include "ansible.h"
#include "syscall.h"

#define _XOPEN_SOURCE 700

#include <sys/ptrace.h>
#include <sys/reg.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>


void trace_by_pid(pid_t pid, FILE *log);

void trace_by_name(const char *name, char **args, FILE *log);

void check_arch(FILE *log);

#endif //PTRACE_PS_TRACE_H
