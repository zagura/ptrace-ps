#define _GNU_SOURCE
#define _XOPEN_SOURCE 700

#include "main.h"


void exit_cleanup(void)
{
    int close_res = 0;
    free(role_dir);
    role_dir = NULL;
    free(process_name);
    process_name = NULL;
    free(output_filename);
    output_filename = NULL;
    if (log_file != stderr && log_file != NULL) {
        sync();
        close_res = fclose(log_file);
        if (close_res != 0) {
            perror("Failed to close log file");
        }
    }
    free(log_filename);
    log_filename = NULL;

}

bool is_dir(const char *path)
{
    if (!path)
        return false;
    struct stat info;
    int status = 0;
    status = stat(path, &info);

    if (status != 0) {
        char msg[LOG_LEN];
        snprintf(msg, LOG_LEN, "Error opening file \'%s\': %s", path, strerror(errno));
        plog_err(log_file, msg);
        return false;
    }
    mode_t mode = info.st_mode;

    if ((mode & S_IFMT) == S_IFDIR)
        return true;

    return false;
}

bool is_regfile(const char *path)
{
    if (!path)
        return false;
    struct stat info;
    int status = 0;
    status = stat(path, &info);

    if (status != 0) {
        char msg[LOG_LEN];
        snprintf(msg, LOG_LEN, "Error opening file \'%s\': %s", path, strerror(errno));
        plog_err(log_file, msg);
        return false;
    }
    mode_t mode = info.st_mode;

    if ((mode & S_IFMT) == S_IFREG)
        return true;

    return false;
}


int main(int argc, char *argv[])
{
    verbose = false;
    int opt_char = 0;
    int tracee_pid = 0;
    log_file = stderr;
    atexit(&exit_cleanup);

    while (opt_char >= 0) {
        int option_index = 0;
        opt_char = getopt_long(argc, argv, short_opts,
                               long_options, &option_index);
        switch (opt_char) {
            case -1:
                break;
            case 0:
                plog_debug(stderr, "Success.\n");
                break;
            case 'p':
                tracee_pid = atoi(optarg);
                break;
            case 'h':
                get_help();
                exit(EXIT_SUCCESS);
            case 'v':
                verbose = true;
                break;
            case 'd':
                role_dir = strdup(optarg);
                if (!role_dir) {
                    exit(EXIT_FAILURE);
                }
                break;
            case 'l':
                log_filename = strdup(optarg);
                if (!log_file) {
                    exit(EXIT_FAILURE);
                }
                break;
            case 'o':
                output_filename = strdup(optarg);
            case '?':
                break;

            default:
                printf("Use -h or --help to get more help.\n");
        }
    }

    if (log_filename) {
        log_file = fopen(log_filename, "a");
        if (!log_file) {
            plog_warn(stderr, "Cannot open log file. Logging to stderr.");
            log_file = stderr;
        }
    }
    plog_init(log_file);
    check_arch(log_file);
    if (!role_dir) {
        role_dir = get_current_dir_name();
        plog_debug(log_file, role_dir);
    }

    if (!is_dir(role_dir)) {
        free(role_dir);
        role_dir = get_current_dir_name();
        plog_info(log_file, "Cannot set ansible directory. Set current dir.");
    }
    ansible_open(role_dir, log_file, output_filename);
    if (tracee_pid > 0) {
        plog_debug(log_file, "Starting tracing based on pid.");
        trace_by_pid(tracee_pid, log_file);
        exit(EXIT_SUCCESS);
    }

    if (tracee_pid <= 0 && optind == argc) {
        char *new_argv[] = {"/bin/bash", "\0"};
        plog_debug(log_file, "No process has been choosen. Taking bash.");
        trace_by_name("/bin/bash", new_argv, log_file);
        exit(EXIT_SUCCESS);
    }
    if (optind < argc) {
        process_name = strdup(argv[optind]);
        if(optind < argc) {
            process_args = &(argv[optind]);
        }
        trace_by_name(process_name, process_args, log_file);
        exit(EXIT_SUCCESS);
    }

    exit(EXIT_FAILURE);
}