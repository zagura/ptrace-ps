//
// Created by misiek on 27.06.17.
//

#ifndef _SYSCALL_H
#define _SYSCALL_H
#define _XOPEN_SOURCE 700
#include "plog.h"
#include "ansible.h"

#include <string.h>
#include <sys/types.h>
#include <stdio.h>
#include <sys/user.h>

char* get_sys_name(unsigned long long int sys_num);
void handle_syscall(unsigned long long int syscall, pid_t pid, FILE *pFILE, struct user_regs_struct *uregs);
void read_syscall(pid_t pid, FILE *log, struct user_regs_struct *uregs);

void write_syscall(pid_t pid, FILE *log, struct user_regs_struct *uregs);

void open_syscall(pid_t pid, FILE *log, struct user_regs_struct *uregs);
#endif //PTRACE_PS_SYSCALL_H
