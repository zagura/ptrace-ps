//
// Created by misiek on 24.06.17.
//


#include "ansible.h"

static FILE* ansible_file = NULL;
void ansible_clean(void) {
    if(ansible_file)
        fclose(ansible_file);
}

void ansible_init(FILE* file, FILE* log) {
    //Add yml format "---"
    //struct in_addr addr = {.s_addr=INADDR_ANY };
    fprintf(file, "---\n");
    fprintf(file, "- name: Tasks generated by ptrace\n");
    fprintf(file, "  hosts: localhost\n");
    fprintf(file, "  tasks:\n");
    plog_debug(log, "Ansible file initialized.");
}

FILE *ansible_open(const char *dir, FILE* log, const char* output) {
    char* filename = calloc(sizeof(char), PATH_MAX);
    if(filename == NULL) {
        plog_warn(log, "Cannot allocate memory.");
    }
    sprintf(filename,"%s/%s", dir, output ? output : "task.yml");
    ansible_file = fopen(filename, "w");
    atexit(&ansible_clean);
    ansible_init(ansible_file, log);
    free(filename);
    return ansible_file;
}

void ansible_file_config(FILE *log, char* path, int mode) {
    if(mode > 0) {
        fprintf(ansible_file, "\n  - file:\n");
        fprintf(ansible_file, "      path: %s\n", path);
        fprintf(ansible_file, "      mode: %#0o\n", mode);
    }else {
        fprintf(ansible_file, "\n  - file:\n");
        fprintf(ansible_file, "      src: %s\n", path);
        fprintf(ansible_file, "      dest: .\n");

    }
    plog_debug(log, "File config saved.");
}





