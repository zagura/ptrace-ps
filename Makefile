CC=gcc
DEBUG=
CFLAGS=$(DEBUG) -Wall -Wextra -std=c11 -pedantic
SRCS=main.c plog.c help.c trace.c ansible.c syscall.c
EXEC=main.out
OBJS=main.o plog.o help.o trace.o ansible.o syscall.o
LFLAGS=

all: clean compile

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@
compile: $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(EXEC) $(LFLAGS)

.PHONY: all clean compile

clean:
	rm -f $(EXEC) $(OBJS)

