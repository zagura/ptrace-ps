//
// Created by misiek on 27.06.17.
//
#define _XOPEN_SOURCE 700
#include <sys/ptrace.h>
#include <fcntl.h>
#include "syscall.h"


/* Source
 * https://filippo.io/linux-syscall-table/
 */
static bool insyscall = false;
static char* syscal_map[] = {
        "read",
        "write",
        "open",
        "close",
        "stat",
        "fstat",
        "lstat",
        "poll",
        "lseek",
        "mmap",
        "mprotect",
        "munmap",
        "brk",
        "rt_sigaction",
        "rt_sigprocmask",
        "rt_sigreturn",
        "ioctl",
        "pread64",
        "pwrite64",
        "readv",
        "writev",
        "access",
        "pipe",
        "select",
        "sched_yield",
        "mremap",
        "msync",
        "mincore",
        "madvise",
        "shmget",
        "shmat",
        "shmctl",
        "dup",
        "dup2",
        "pause",
        "nanosleep",
        "getitimer",
        "alarm",
        "setitimer",
        "getpid",
        "sendfile",
        "socket",
        "connect",
        "accept",
        "sendto",
        "recvfrom",
        "sendmsg",
        "recvmsg",
        "shutdown",
        "bind",
        "listen",
        "getsockname",
        "getpeername",
        "socketpair",
        "setsockopt",
        "getsockopt",
        "clone",
        "fork",
        "vfork",
        "execve",
        "exit",
        "wait4",
        "kill",
        "uname",
        "semget",
        "semop",
        "semctl",
        "shmdt",
        "msgget",
        "msgsnd",
        "msgrcv",
        "msgctl",
        "fcntl",
        "flock",
        "fsync",
        "fdatasync",
        "truncate",
        "ftruncate",
        "getdents",
        "getcwd",
        "chdir",
        "fchdir",
        "rename",
        "mkdir",
        "rmdir",
        "creat",
        "link",
        "unlink",
        "symlink",
        "readlink",
        "chmod",
        "fchmod",
        "chown",
        "fchown",
        "lchown",
        "umask",
        "gettimeofday",
        "getrlimit",
        "getrusage",
        "sysinfo",
        "times",
        "ptrace",
        "getuid",
        "syslog",
        "getgid",
        "setuid",
        "setgid",
        "geteuid",
        "getegid",
        "setpgid",
        "getppid",
        "getpgrp",
        "setsid",
        "setreuid",
        "setregid",
        "getgroups",
        "setgroups",
        "setresuid",
        "getresuid",
        "setresgid",
        "getresgid",
        "getpgid",
        "setfsuid",
        "setfsgid",
        "getsid",
        "capget",
        "capset",
        "rt_sigpending",
        "rt_sigtimedwait",
        "rt_sigqueueinfo",
        "rt_sigsuspend",
        "sigaltstack",
        "utime",
        "mknod",
        "uselib",
        "personality",
        "ustat",
        "statfs",
        "fstatfs",
        "sysfs",
        "getpriority",
        "setpriority",
        "sched_setparam",
        "sched_getparam",
        "sched_setscheduler",
        "sched_getscheduler",
        "sched_get_priority_max",
        "sched_get_priority_min",
        "sched_rr_get_interval",
        "mlock",
        "munlock",
        "mlockall",
        "munlockall",
        "vhangup",
        "modify_ldt",
        "pivot_root",
        "_sysctl",
        "prctl",
        "arch_prctl",
        "adjtimex",
        "setrlimit",
        "chroot",
        "sync",
        "acct",
        "settimeofday",
        "mount",
        "umount2",
        "swapon",
        "swapoff",
        "reboot",
        "sethostname",
        "setdomainname",
        "iopl",
        "ioperm",
        "create_module",
        "init_module",
        "delete_module",
        "get_kernel_syms",
        "query_module",
        "quotactl",
        "nfsservctl",
        "getpmsg",
        "putpmsg",
        "afs_syscall",
        "tuxcall",
        "security",
        "gettid",
        "readahead",
        "setxattr",
        "lsetxattr",
        "fsetxattr",
        "getxattr",
        "lgetxattr",
        "fgetxattr",
        "listxattr",
        "llistxattr",
        "flistxattr",
        "removexattr",
        "lremovexattr",
        "fremovexattr",
        "tkill",
        "time",
        "futex",
        "sched_setaffinity",
        "sched_getaffinity",
        "set_thread_area",
        "io_setup",
        "io_destroy",
        "io_getevents",
        "io_submit",
        "io_cancel",
        "get_thread_area",
        "lookup_dcookie",
        "epoll_create",
        "epoll_ctl_old",
        "epoll_wait_old",
        "remap_file_pages",
        "getdents64",
        "set_tid_address",
        "restart_syscall",
        "semtimedop",
        "fadvise64",
        "timer_create",
        "timer_settime",
        "timer_gettime",
        "timer_getoverrun",
        "timer_delete",
        "clock_settime",
        "clock_gettime",
        "clock_getres",
        "clock_nanosleep",
        "exit_group",
        "epoll_wait",
        "epoll_ctl",
        "tgkill",
        "utimes",
        "vserver",
        "mbind",
        "set_mempolicy",
        "get_mempolicy",
        "mq_open",
        "mq_unlink",
        "mq_timedsend",
        "mq_timedreceive",
        "mq_notify",
        "mq_getsetattr",
        "kexec_load",
        "waitid",
        "add_key",
        "request_key",
        "keyctl",
        "ioprio_set",
        "ioprio_get",
        "inotify_init",
        "inotify_add_watch",
        "inotify_rm_watch",
        "migrate_pages",
        "openat",
        "mkdirat",
        "mknodat",
        "fchownat",
        "futimesat",
        "newfstatat",
        "unlinkat",
        "renameat",
        "linkat",
        "symlinkat",
        "readlinkat",
        "fchmodat",
        "faccessat",
        "pselect6",
        "ppoll",
        "unshare",
        "set_robust_list",
        "get_robust_list",
        "splice",
        "tee",
        "sync_file_range",
        "vmsplice",
        "move_pages",
        "utimensat",
        "epoll_pwait",
        "signalfd",
        "timerfd_create",
        "eventfd",
        "fallocate",
        "timerfd_settime",
        "timerfd_gettime",
        "accept4",
        "signalfd4",
        "eventfd2",
        "epoll_create1",
        "dup3",
        "pipe2",
        "inotify_init1",
        "preadv",
        "pwritev",
        "rt_tgsigqueueinfo",
        "perf_event_open",
        "recvmmsg",
        "fanotify_init",
        "fanotify_mark",
        "prlimit64",
        "name_to_handle_at",
        "open_by_handle_at",
        "clock_adjtime",
        "syncfs",
        "sendmmsg",
        "setns",
        "getcpu",
        "process_vm_readv",
        "process_vm_writev",
        "kcmp",
        "finit_module",
        "sched_setattr",
        "sched_getattr",
        "renameat2",
        "seccomp",
        "getrandom",
        "memfd_create",
        "kexec_file_load",
        "bpf",
        "execveat",
        "userfaultfd",
        "membarrier",
        "mlock2",
        "copy_file_range",
        "preadv2",
        "pwritev2",
        "pkey_mprotect",
        "pkey_alloc",
        "pkey_free",
        "statx",
        "rt_sigaction",
        "rt_sigreturn",
        "ioctl",
        "readv",
        "writev",
        "recvfrom",
        "sendmsg",
        "recvmsg",
        "execve",
        "ptrace",
        "rt_sigpending",
        "rt_sigtimedwait",
        "rt_sigqueueinfo",
        "sigaltstack",
        "timer_create",
        "mq_notify",
        "kexec_load",
        "waitid",
        "set_robust_list",
        "get_robust_list",
        "vmsplice",
        "move_pages",
        "preadv",
        "pwritev",
        "rt_tgsigqueueinfo",
        "recvmmsg",
        "sendmmsg",
        "process_vm_readv",
        "process_vm_writev",
        "setsockopt",
        "getsockopt",
        "io_setup",
        "io_submit",
        "execveat",
        "preadv2",
        "pwritev2"
};
static void (*vec[]) (pid_t, FILE*, struct user_regs_struct*) = {
        &read_syscall,
        &write_syscall,
        &open_syscall,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL

};

char* get_sys_name(unsigned long long int sys_num) {
    if (sys_num > 547) {
        return NULL;
    }
    if (sys_num > 332) {
        sys_num -= 179;
    }

    // remember to free after use...
    return strdup(syscal_map[sys_num]);
}

void get_str_trace(pid_t pid,char* addr, FILE* log, size_t size, char* dest) {
    char* result = dest;
    int offset = 0;
    for(offset = 0; offset < size; offset++) {
        long pres = ptrace(PTRACE_PEEKDATA, pid,
                           addr+offset*sizeof(long), NULL);
        memcpy(result + offset*sizeof(long), &pres, sizeof(long));
        if (pres == -1) {
            log_format(plog_err, log, "Cannot get data in open syscall.");
        }
        if(result[(offset + 1)*sizeof(long) - 1] == '\0')
            return;
    }
}
void handle_syscall(unsigned long long int syscall_num, pid_t pid, FILE *log, struct user_regs_struct* uregs) {
    static const unsigned long long int size = 9;
    if(!insyscall) {
        insyscall = !insyscall;
        if (syscall_num >= size) {
            plog_debug(log, "Unimplemented syscall.");
            return;
        }
        if (vec[syscall_num] != NULL) {
            plog_debug(log, "Handling syscall.");
            vec[syscall_num](pid, log, uregs);
        }
    }else{
        insyscall = !insyscall;
        log_format(plog_debug, log, "Exited syscall with ret value: 0x%llx.", uregs->rax);
    }
/*    switch(syscall_num){
        case 0:
            read_syscall(pid, log, uregs);
            break;
        case 1:
            write_syscall(pid, log, uregs);
            break;
        case 2:
            open_syscall(pid, log, uregs);*/

}

void read_syscall(pid_t pid, FILE *log, struct user_regs_struct *uregs) {
    /* read(int fd, void* data, size_t count) */
    int fd = (int) uregs->rdi;
    void* data = (void *) uregs->rsi;
    size_t count = uregs->rdx;
    log_format(plog_debug, log, "read(%d, %p, %lu)", fd, data, count);

}

void write_syscall(pid_t pid, FILE *log, struct user_regs_struct *uregs) {

}

void open_syscall(pid_t pid, FILE *log, struct user_regs_struct *uregs){
    /* int open(const char *pathname, int flags);
     * int open(const char *pathname, int flags, mode_t mode);
     * int creat(const char *pathname, mode_t mode); */
    char* pathname = (char* )uregs->rdi;
    int flags = (int) uregs->rsi;
    char *path = calloc(PATH_MAX,sizeof(char));
    get_str_trace(pid, pathname, log, PATH_MAX, path);
    int user, group, other;
    user = (flags & S_IRUSR) | (flags & S_IWUSR) | (flags & S_IXUSR);
    group = (flags & S_IRGRP) | (flags & S_IWGRP) | (flags & S_IXGRP);
    other = (flags & S_IROTH) | (flags & S_IWOTH) | (flags & S_IXOTH);

    log_format(plog_debug, log, "open(%p, %d).", (void*)pathname, flags);
    log_format(plog_debug, log, "File mode u=%#0o, g=%#0o, o=%#0o", user, group, other);
    log_format(plog_debug, log, "open at path: %s", path);
    int mode;
    if (flags != 524288 && (flags & O_CREAT)) {
        mode = flags & (S_IRWXU | S_IRWXG | S_IRWXO);
        ansible_file_config(log, path, mode);
    }
    free(path);

}


